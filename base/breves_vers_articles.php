<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

function breves_vers_articles_declarer_tables_auxiliaires($tables_auxiliaires) {
	$spip_breves_articles = array(
		"id_breve" => "int(11) NOT NULL",
		"id_article" => "int(11) NOT NULL"
	);

	$spip_breves_articles_key = array(
		"PRIMARY KEY" => "id_breve, id_article"
	);

	$tables_auxiliaires['spip_breves_articles'] = array(
		'field' => &$spip_breves_articles,
		'key' => &$spip_breves_articles_key
	);

	return $tables_auxiliaires;

}
