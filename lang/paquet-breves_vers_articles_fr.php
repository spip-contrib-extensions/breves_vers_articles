<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file

// Fichier produit par PlugOnet
// Module: paquet-breves_vers_articles
// Langue: fr
// Date: 21-06-2023 15:18:27
// Items: 2

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'breves_vers_articles_description' => 'Transforme les brèves d\'un site en articles, les range dans une rubrique, leur attribue un auteur, fait suivre les logos et les forums, et convertit les liens raccourcis, permet d\'assurer la redirection vis-à-vis des moteurs de recherche.',
	'breves_vers_articles_slogan' => 'Convertir des brèves en articles',
);
?>