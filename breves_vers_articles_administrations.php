<?php
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

function breves_vers_articles_upgrade($nom_meta_base_version, $version_cible) {
	$maj = [];
	$maj['create'] = [
		['maj_tables', [TABLE_BREVES_ARTICLES]],
	];

	include_spip('base/upgrade');
	maj_plugin($nom_meta_base_version, $version_cible, $maj);
}

function breves_vers_articles_vider_tables($nom_meta_base_version) {
	sql_drop_table('spip_breves_articles');
	effacer_meta($nom_meta_base_version);
}
